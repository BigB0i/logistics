package trucks.truckers4.Controllers;

import static org.junit.jupiter.api.Assertions.*;

import org.hibernate.annotations.Check;
import org.junit.Test;
import trucks.truckers4.Model.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class HomeViewTest {

    @Test
    public void getDestinationsWithFilterStatus() {
        HomeView homeView = new HomeView();

        List<Destination> destinations = new ArrayList<>();
        List<Checkpoint> checkpoints = new ArrayList<>();

        LocalDate departure = LocalDate.parse("2023-01-15");
        Checkpoint checkpoint = new Checkpoint("Adresas", null, departure);

        LocalDate arrival = LocalDate.parse("2023-01-20");
        Checkpoint checkpoint1 = new Checkpoint("Adresas", arrival, null);

        checkpoints.add(checkpoint);
        checkpoints.add(checkpoint1);

        Truck truck = new Truck();
        Driver driver = new Driver();
        Manager manager = new Manager();
        Cargo cargo = new Cargo();

        Status statusPending = Status.PENDING;
        Status statusDelivered = Status.DELIVERED;

        Destination destination = new Destination(truck, checkpoints, driver, manager, cargo, statusPending);
        Destination destination1 = new Destination(truck, checkpoints, driver, manager, cargo, statusDelivered);

        destinations.add(destination);
        destinations.add(destination1);

        LocalDate start = LocalDate.parse("2023-01-01");
        LocalDate end = LocalDate.parse("2023-02-01");

        Destination result = homeView.getDestinationsWithFilter(destinations, start, end, statusPending).get(0);

        assertEquals(destination, result);
    }

    @Test
    public void getDestinationsWithFilterDate() {
        HomeView homeView = new HomeView();

        List<Destination> destinations = new ArrayList<>();
        List<Checkpoint> checkpoints = new ArrayList<>();

        LocalDate departure = LocalDate.parse("2023-01-15");
        Checkpoint checkpoint = new Checkpoint("Adresas", null, departure);

        LocalDate arrival = LocalDate.parse("2023-01-20");
        Checkpoint checkpoint1 = new Checkpoint("Adresas", arrival, null);

        checkpoints.add(checkpoint);
        checkpoints.add(checkpoint1);

        Truck truck = new Truck();
        Driver driver = new Driver();
        Manager manager = new Manager();
        Cargo cargo = new Cargo();

        Status statusPending = Status.PENDING;
        Status statusDelivered = Status.DELIVERED;

        Destination destination = new Destination(truck, checkpoints, driver, manager, cargo, statusPending);
        Destination destination1 = new Destination(truck, checkpoints, driver, manager, cargo, statusDelivered);

        destinations.add(destination);
        destinations.add(destination1);

        LocalDate start = LocalDate.parse("2023-01-01");
        LocalDate end = LocalDate.parse("2023-01-10");

        List<Destination> result = homeView.getDestinationsWithFilter(destinations, start, end, statusPending);

        assertEquals(0, result.size());
    }
}