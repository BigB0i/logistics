package trucks.truckers4.Model;

public enum Status {
    PENDING,
    ENROUTE,
    DELIVERED
}
