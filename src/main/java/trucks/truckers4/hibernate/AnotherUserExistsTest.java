package trucks.truckers4.hibernate;

import org.junit.Test;

import org.junit.Assert;
import trucks.truckers4.Model.Driver;


import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;


public class AnotherUserExistsTest {

    @Test
    public void anotherUserExists() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Truckers");
        UserHib userHib = new UserHib(entityManagerFactory);
        Driver driver = new Driver("Mariukas", "Pasalpanosis", "868686868", "Mariuxas@gmai.ru","Mariuxas", "Mariux123", LocalDate.parse("2000-01-01"));
        userHib.createUser(driver);
        Assert.assertTrue( userHib.anotherUserExists("Mariuxas"));
    }
    @Test
    public void anotherUserDoesntExists() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Truckers");
        UserHib userHib = new UserHib(entityManagerFactory);

        Assert.assertTrue( userHib.anotherUserExists("123"));
    }

}