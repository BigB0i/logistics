package trucks.truckers4.hibernate;

import static org.junit.Assert.*;

import org.junit.Test;
import trucks.truckers4.Model.*;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

public class CommentHibTest {

    @Test
    public void testCommentCreateDeletePass() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Truckers");
        User user = new Driver("user", "user", "+3702222222", "testuser@gmail.com", "username", "password", LocalDate.parse("2000-01-01"));
        UserHib userHib = new UserHib(entityManagerFactory);
        userHib.createUser(user);

        Forum forum = new Forum(user, "haha", "what is going on", LocalDate.now());
        ForumHib forumHib = new ForumHib(entityManagerFactory);
        forumHib.createForum(forum);

        CommentHib commentHib = new CommentHib(entityManagerFactory);
        Comment comment = new Comment(user, "haha", forum, LocalDate.now());
        commentHib.createComment(comment);
        Comment temp1 = commentHib.getCommentById(comment.getId());
        comment = new Comment(user, "hahaha1", forum, LocalDate.now());
        commentHib.createComment(comment);
        Comment temp2 = commentHib.getCommentById(comment.getId());

        assertNotEquals(temp1, temp2);

    }


    @Test
    public void testCommentCreateDeletePassFail() {
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Truckers");
        User user = new Driver("user", "user", "+3702222222", "testuser@gmail.com", "username", "password", LocalDate.parse("2000-01-01"));
        UserHib userHib = new UserHib(entityManagerFactory);
        userHib.createUser(user);
        Forum forum = new Forum(user, "haha", "what is going on", LocalDate.now());
        ForumHib forumHib = new ForumHib(entityManagerFactory);
        forumHib.createForum(forum);
        CommentHib commentHib = new CommentHib(entityManagerFactory);
        Comment comment = new Comment(user, "haha", forum, LocalDate.now());
        commentHib.createComment(comment);
        commentHib.createComment(comment);
        Comment temp1 = commentHib.getCommentById(comment.getId());
        Comment temp2 = commentHib.getCommentById(comment.getId());
        assertEquals(temp1, temp2);
    }
}