package trucks.truckers4.hibernate;

import org.junit.Assert;
import org.junit.Test;
import trucks.truckers4.Model.Driver;
import trucks.truckers4.Model.Manager;
import trucks.truckers4.Model.User;

import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.time.LocalDate;

import static org.junit.Assert.*;

public class UserHibTest {
    @Test
    public void deleteDriverTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Truckers");
        UserHib userHib = new UserHib(entityManagerFactory);
        Driver driver = new Driver("DRIVER", "TEST", "868686868", "TEST@gmai.ru","TEST", "TEST", LocalDate.parse("2000-01-01"));
        userHib.createUser(driver);
        Driver temp = userHib.getDriverById(driver.getId());
        userHib.deleteUser(temp);
        assertNotEquals(driver,temp);
    }

    @Test
    public void deleteManagerTest(){
        EntityManagerFactory entityManagerFactory = Persistence.createEntityManagerFactory("Truckers");
        UserHib userHib = new UserHib(entityManagerFactory);
        Manager manager = new Manager("MANAGER", "zz", "86868", "manager@gmai.ru","Manager", "Mangaer", LocalDate.parse("2000-01-01"));
        userHib.createUser(manager);
        Manager temp = userHib.getManagerById(manager.getId());
        userHib.deleteUser(temp);
        assertNotEquals(manager,temp);
    }


}